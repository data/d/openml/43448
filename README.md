# OpenML dataset: Calculate-Concrete-Strength

https://www.openml.org/d/43448

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This data set aims to provide a start up for the ones who just started off with deep learning and act as a benchmark.
Content
The feature set includes:

Cement
Blast Furnace Slag
Fly Ash
Water
Super-plasticizer
Coarse Aggregate
Fine Aggregate
Age

The target set is:

Strength of the Cement

Inspiration
The main agenda is not only to solve and get better results but understand the process and learn from the journey.
best of luck.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43448) of an [OpenML dataset](https://www.openml.org/d/43448). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43448/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43448/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43448/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

